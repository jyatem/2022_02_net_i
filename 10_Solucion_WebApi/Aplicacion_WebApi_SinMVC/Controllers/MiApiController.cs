﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Aplicacion_WebApi_SinMVC.Controllers
{
    public class MiApiController : ApiController
    {
        // GET: api/MiApi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/MiApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/MiApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/MiApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/MiApi/5
        public void Delete(int id)
        {
        }
    }
}
