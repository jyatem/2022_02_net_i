﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Interfaces
{
    public class MiClase : IMiInterfaz
    {
        public int DuplicarValor(int valor)
        {
            return valor * 2;
        }

        public void MetodoDeLaInterfaz()
        {
            Console.WriteLine("Método de la interfaz implementado");
        }
    }
}
