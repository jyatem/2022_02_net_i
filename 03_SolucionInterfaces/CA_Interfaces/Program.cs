﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Interfaces
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IMiInterfaz miInterfaz = new MiClase();
            miInterfaz.MetodoDeLaInterfaz();

            MiClase miClase = new MiClase();
            Console.WriteLine(miClase.DuplicarValor(10));

            Console.ReadLine();
        }
    }
}
