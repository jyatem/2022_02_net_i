﻿using LibreriaCalculadora;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace LibreriaCalculadoraTest
{
    [TestClass]
    public class CalculadoraTest
    {
        [TestMethod]
        public void Sumar_Enteros()
        {
            // AAA
            // Arrange
            Calculadora calculadora = new Calculadora();

            // Act
            int resultado = calculadora.Sumar(2, 3);

            // Assert
            Assert.AreEqual(5, resultado);
        }

        [TestMethod]
        public void Sumar_Enteros_Negativos()
        {
            // AAA
            // Arrange
            Calculadora calculadora = new Calculadora();

            // Act
            int resultado = calculadora.Sumar(-10, -5);

            // Assert
            Assert.AreEqual(-15, resultado);
        }

        [TestMethod]
        public void Dividir_Enteros()
        {
            // AAA
            // Arrange
            Calculadora calculadora = new Calculadora();

            // Act
            int resultado = calculadora.Dividir(8, 4);

            // Assert
            Assert.AreEqual(2, resultado);
        }

        [TestMethod]
        public void Dividir_NumeradorCero()
        {
            // AAA
            // Arrange
             Calculadora calculadora = new Calculadora();

            // Act
            int resultado = calculadora.Dividir(100, 2);

            // Assert
            Assert.AreEqual(50, resultado);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void Dividir_DenominadorCero()
        {
            // AAA
            // Arrange
            Calculadora calculadora = new Calculadora();

            // Act
            int resultado = calculadora.Dividir(8, 0);
        }
    }
}
