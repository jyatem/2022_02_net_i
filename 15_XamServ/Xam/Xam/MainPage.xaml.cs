﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xam.Models;
using Xam.Service;
using Xamarin.Forms;

namespace Xam
{
    public partial class MainPage : ContentPage
    {
        private RestService _restService;

        public MainPage()
        {
            InitializeComponent();
            _restService = new RestService();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            List<Repository> repositories = await _restService.ObtenerRepositoriosAsync(Constantes.URLGitHubReposEndPoint);

            collectionView.ItemsSource = repositories;
        }
    }
}
