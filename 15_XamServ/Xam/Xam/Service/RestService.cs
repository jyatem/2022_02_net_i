﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xam.Models;
using Xamarin.Forms;

namespace Xam.Service
{
    public class RestService
    {
        private HttpClient _httpClient;

        public RestService()
        {
            _httpClient = new HttpClient();

            if (Device.RuntimePlatform == Device.UWP)
            {
                _httpClient.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            }
        }

        // Async GetRepositoriesAsyn. Retorna un listado Repository. Parametro de entrada la URL.
        public async Task<List<Repository>> ObtenerRepositoriosAsync(string url)
        {
            List<Repository> repositories = new List<Repository>();

            try
            {
                HttpResponseMessage httpResponseMessage = await _httpClient.GetAsync(url);

                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    string resultado = await httpResponseMessage.Content.ReadAsStringAsync();

                    repositories = JsonConvert.DeserializeObject<List<Repository>>(resultado);
                }
            }
            catch (Exception ex)
            {
                Debug.Write("Error: {0}", ex.Message);
            }

            return repositories;
        }
    }
}
