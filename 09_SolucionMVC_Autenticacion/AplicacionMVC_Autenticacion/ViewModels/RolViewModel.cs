﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AplicacionMVC_Autenticacion.ViewModels
{
    public class RolViewModel
    {
        public string Id { get; set; }

        [Required]
        [Display(Name = "Rol")]
        public string Nombre { get; set; }
    }
}