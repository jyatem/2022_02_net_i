﻿using AplicacionMVC_Autenticacion.Models;
using AplicacionMVC_Autenticacion.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicacionMVC_Autenticacion.Controllers
{
    public class RolesController : Controller
    {
        // CRUD Tabla de AspNetRoles
        private RoleManager<IdentityRole> gestorRoles = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
        
        // GET: Roles
        public ActionResult Index()
        {
            return View(gestorRoles.Roles.Select(rol => new RolViewModel { Id = rol.Id, Nombre = rol.Name }));
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear(RolViewModel rolViewModel)
        {
            if (ModelState.IsValid)
            {
                if (!gestorRoles.RoleExists(rolViewModel.Nombre))
                {
                    gestorRoles.Create(new IdentityRole(rolViewModel.Nombre));
                }

                return RedirectToAction("Index");
            }

            return View(rolViewModel);
        }
    }
}