﻿using AplicacionMVC_Autenticacion.Models;
using AplicacionMVC_Autenticacion.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AplicacionMVC_Autenticacion.Controllers
{
    public class UsuariosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private UserManager<ApplicationUser> gestorUsuarios = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

        // GET: Usuarios
        public ActionResult Index()
        {
            return View(gestorUsuarios.Users.Select(u => new UserViewModel { Email = u.Email, Nombre = u.UserName, Id = u.Id }));
        }

        public ActionResult Roles(string id)
        {
            UserViewModel userViewModel = CargarInformacion(id);
            return View(userViewModel);
        }

        public ActionResult AgregarRol(string userId)
        {
            if (String.IsNullOrEmpty(userId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = db.Users.Find(userId);

            if (user == null)
            {
                return HttpNotFound();
            }

            UserViewModel userViewModel = new UserViewModel
            {
                Id = user.Id,
                Nombre = user.UserName,
                Email = user.Email
            };

            ViewBag.RolId = new SelectList(db.Roles.OrderBy(r => r.Name), "Id", "Name");

            return View(userViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarRol(UserViewModel userViewModel)
        {
            string rolId = Request["RolId"];

            if (string.IsNullOrEmpty(rolId))
            {
                ViewBag.RolId = new SelectList(db.Roles.OrderBy(r => r.Name), "Id", "Name");
                return View(userViewModel);
            }

            var rol = db.Roles.Find(rolId);

            if (!gestorUsuarios.IsInRole(userViewModel.Id, rol.Name))
            {
                gestorUsuarios.AddToRole(userViewModel.Id, rol.Name);
            }

            userViewModel = CargarInformacion(userViewModel.Id);
            return View("Roles", userViewModel);
        }

        private UserViewModel CargarInformacion(string userId)
        {
            // AspNetUsers y AspNetRoles

            // Obtener información del usuario, creamos un objeto ViewModel para la View
            var usuario = db.Users.Find(userId);

            if (usuario == null) return null;

            UserViewModel userViewModel = new UserViewModel
            {
                Id = usuario.Id,
                Nombre = usuario.UserName,
                Email = usuario.Email,
                Roles = new List<RolViewModel>()
            };

            // Obtener información del rol, especificamente el nombre del rol
            foreach (var role in usuario.Roles)
            {
                var rol = db.Roles.Find(role.RoleId);

                RolViewModel rolViewModel = new RolViewModel
                {
                    Id = role.RoleId,
                    Nombre = rol.Name
                };

                userViewModel.Roles.Add(rolViewModel);
            }

            return userViewModel;
        }
    }
}