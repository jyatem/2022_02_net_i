﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AplicacionMVC_Autenticacion.Startup))]
namespace AplicacionMVC_Autenticacion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
