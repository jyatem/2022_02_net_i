﻿internal class Program
{
    private static void Main(string[] args)
    {
        // Console.WriteLine("Hola, mundo!");

        // Console.WriteLine("Ingresa tu nombre:");
        // string? nombre = Console.ReadLine();
        var fechaActual = DateTime.Now;

        // Console.WriteLine($"{Environment.NewLine}Hola {nombre}, hoy {fechaActual:d} a las {fechaActual:t}!");

        Persona persona = new Persona()
        {
            Id = 1,
            Nombre = "Jairo"
        };

        Console.WriteLine($"{Environment.NewLine}Hola {persona.Nombre} ({persona.Id}), hoy {fechaActual:d} a las {fechaActual:t}!");

        Console.WriteLine($"{Environment.NewLine}Presione una tecla para continuar...");
        Console.ReadKey(true);
    }
}