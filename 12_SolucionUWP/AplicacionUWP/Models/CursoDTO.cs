﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionUWP.Models
{
    public class CursoDTO
    {
        public int Id { get; set; }

        public string NombreCurso { get; set; }

        public string NombrePrograma { get; set; }
    }
}
