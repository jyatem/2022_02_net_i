﻿using AplicacionUWP.DataAcces;
using AplicacionUWP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AplicacionUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CursoDetallePage : Page
    {
        private CursoDA _cursoDA;
        private int _id;

        public CursoDetallePage()
        {
            this.InitializeComponent();
            _cursoDA = new CursoDA();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _id = Convert.ToInt32(e.Parameter);
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //var dialog = new MessageDialog(_id.ToString());
            //await dialog.ShowAsync();

            try
            {
                Curso curso = await _cursoDA.ObtenerCurso(_id);

                txtId.Text = curso.Id.ToString();
                txtNombre.Text = curso.NombreCurso;
                dpFecha.SelectedDate = curso.Fecha;
                txtIdPrograma.Text = curso.ProgramaId.ToString();
            }
            catch (Exception ex)
            {
                var dialogo = new MessageDialog(ex.Message);
                await dialogo.ShowAsync();
            }
        }

        private async void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            ContentDialog contentDialog = new ContentDialog
            {
                Title = "Eliminar",
                Content = "¿Realmente desea eliminar el curso",
                PrimaryButtonText = "No",
                SecondaryButtonText = "Sí",
                DefaultButton = ContentDialogButton.Primary
            };

            ContentDialogResult resultado = await contentDialog.ShowAsync();

            if (resultado == ContentDialogResult.Secondary)
            {
                //var dialog = new MessageDialog("Eliminar");
                //await dialog.ShowAsync();
                
                string resutaldo = await _cursoDA.EliminarCurso(_id);
                var dialog = new MessageDialog("Curso eliminado exitosamente");
                await dialog.ShowAsync();
            }
        }

        private async void btnActualizar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Curso cursoActualizar = new Curso
                {
                    Id = Convert.ToInt32(txtId.Text),
                    NombreCurso = txtNombre.Text,
                    Fecha = dpFecha.Date.DateTime,
                    ProgramaId = Convert.ToInt32(txtIdPrograma.Text)
                };

                string resultado = await _cursoDA.ActualizarCurso(cursoActualizar);

                var dialogo = new MessageDialog($"Se actualizó el curso {cursoActualizar.NombreCurso} con Id {cursoActualizar.Id}");
                await dialogo.ShowAsync();
            }
            catch (Exception ex)
            {
                var dialogo = new MessageDialog(ex.Message);
                await dialogo.ShowAsync();
            }
        }
    }
}
