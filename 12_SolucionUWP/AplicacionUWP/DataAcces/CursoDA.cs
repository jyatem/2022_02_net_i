﻿using AplicacionUWP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.AllJoyn;

namespace AplicacionUWP.DataAcces
{
    public class CursoDA
    {
        private readonly HttpClient httpClient = new HttpClient();

        public CursoDA()
        {
            httpClient.BaseAddress = new Uri("http://localhost:5189/");
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        //public async Task<string> ObtenerCursos()
        public async Task<List<CursoDTO>> ObtenerCursos()
        {
            HttpResponseMessage httpResponseMessage = await httpClient.GetAsync("/api/Cursos");

            string respuesta = "";

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                respuesta = await httpResponseMessage.Content.ReadAsStringAsync();
                //return respuesta;

                var cursos = JsonConvert.DeserializeObject<List<CursoDTO>>(respuesta);
                return cursos;
            }
            else
            {
                throw new Exception("Error: " + httpResponseMessage.StatusCode);
            }
        }

        public async Task<Curso> IngresarCurso(Curso curso)
        {
            string jsonCurso = JsonConvert.SerializeObject(curso);

            StringContent data = new StringContent(jsonCurso, Encoding.UTF8, "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.PostAsync("/api/Cursos", data);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                string respuesta = await httpResponseMessage.Content.ReadAsStringAsync();

                Curso cursoIngresado = JsonConvert.DeserializeObject<Curso>(respuesta);

                return cursoIngresado;
            }
            else
            {
                throw new Exception("Error");
            }
        }

        public async Task<Curso> ObtenerCurso(int idCurso)
        {
            HttpResponseMessage httpResponseMessage = await httpClient.GetAsync($"/api/Cursos/{idCurso}");

            string respuesta = "";

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                respuesta = await httpResponseMessage.Content.ReadAsStringAsync();
                Curso curso = JsonConvert.DeserializeObject<Curso>(respuesta);
                return curso;
            }
            else
            {
                throw new Exception("Error: " + httpResponseMessage.StatusCode);
            }
        }

        public async Task<string> EliminarCurso(int idCurso)
        {
            HttpResponseMessage httpResponseMessage = await httpClient.DeleteAsync($"/api/Cursos/{idCurso}");

            string respuesta = "";

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                respuesta = await httpResponseMessage.Content.ReadAsStringAsync();
                return respuesta;
            }
            else
            {
                throw new Exception("Error: " + httpResponseMessage.StatusCode);
            }
        }

        public async Task<string> ActualizarCurso(Curso curso)
        {
            string jsonCurso = JsonConvert.SerializeObject(curso);

            StringContent data = new StringContent(jsonCurso, Encoding.UTF8, "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.PutAsync($"/api/Cursos/{curso.Id}", data);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                string respuesta = await httpResponseMessage.Content.ReadAsStringAsync();
                return respuesta;
            }
            else
            {
                throw new Exception("Error");
            }
        }
    }
}
