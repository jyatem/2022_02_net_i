﻿using AplicacionUWP.DataAcces;
using AplicacionUWP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AplicacionUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CursoInsertarPage : Page
    {
        private CursoDA _cursoDA;

        public CursoInsertarPage()
        {
            this.InitializeComponent();
            _cursoDA = new CursoDA();
        }

        private async void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Curso cursoIngresar = new Curso
                {
                    NombreCurso = txtNombre.Text,
                    Fecha = dpFecha.Date.DateTime,
                    ProgramaId = Convert.ToInt32(txtIdPrograma.Text)
                };

                Curso curso = await _cursoDA.IngresarCurso(cursoIngresar);

                var dialogo = new MessageDialog($"Se ingresó el curso {curso.NombreCurso} con Id {curso.Id}");
                await dialogo.ShowAsync();
            }
            catch (Exception ex)
            {
                var dialogo = new MessageDialog(ex.Message);
                await dialogo.ShowAsync();
            }
        }
    }
}
