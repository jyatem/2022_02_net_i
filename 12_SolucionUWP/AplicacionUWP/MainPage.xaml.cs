﻿using AplicacionUWP.DataAcces;
using AplicacionUWP.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace AplicacionUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private CursoDA _cursoDA;

        public MainPage()
        {
            this.InitializeComponent();
            _cursoDA = new CursoDA();
            CargarCursos();
        }
        private void btnRefrescar_Click(object sender, RoutedEventArgs e)
        {
            CargarCursos();
        }

        private async void CargarCursos()
        {
            progress.IsActive = true;
            progress.Visibility = Visibility.Visible;

            try
            {
                //string informacion = await _cursoDA.ObtenerCursos();
                //var dialogo = new MessageDialog(informacion);

                List<CursoDTO> cursos = await _cursoDA.ObtenerCursos();
                //var dialogo = new MessageDialog(cursos[0].NombreCurso);
                //await dialogo.ShowAsync();
                listadoCursos.ItemsSource = cursos;
            }
            catch (Exception ex)
            {
                var dialogo = new MessageDialog(ex.Message);
                await dialogo.ShowAsync();
            }
            finally
            {
                progress.IsActive = false;
                progress.Visibility = Visibility.Collapsed;
            }
        }

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(CursoInsertarPage));
        }

        //private async void listadoCursos_ItemClick(object sender, ItemClickEventArgs e)
        private void listadoCursos_ItemClick(object sender, ItemClickEventArgs e)
        {
            CursoDTO cursoDTO = e.ClickedItem as CursoDTO;

            this.Frame.Navigate(typeof(CursoDetallePage), cursoDTO.Id);

            //var dialogo = new MessageDialog(cursoDTO.Id.ToString());
            //await dialogo.ShowAsync();
        }
    }
}
