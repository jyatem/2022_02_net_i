﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_ManejoExcepciones
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //int i = 0;
                //int j = 10 / i;

                //string dato = String.Empty;
                string dato = "";

                Funcion(dato);

                Console.WriteLine("Operación terminada exitosamente");
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            finally
            {
                Console.WriteLine("Este código se ejecuta así el código este bien o no");
            }

            Console.ReadLine();
        }

        static void Funcion(string s)
        {
            if (String.IsNullOrEmpty(s))
            {
                throw new Exception("El valor del parametro de entrada es nula o vacia");
            }

            Console.WriteLine(s);
        }
    }
}
