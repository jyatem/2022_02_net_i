﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_MetodosEstaticos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int resultado = Calculadora.Sumar(10, 14);
            Console.WriteLine(resultado);

            // Crear un método estatico que determine si un número es par o impar. Listado de números enteros positivos. Recorrerlo y van a mostrar por consola si ese número es par o impar. Modulo %

            List<int> lista = new List<int> { 2, 5, 13, 80, 121, 4345 };

            for (int i = 0; i < lista.Count; i++)
            {
                Console.WriteLine("El número {0} es {1}", lista[i], Calculadora.EsPar(lista[i]) ? "par" : "impar");
            }

            foreach (int numero in lista)
            {
                Console.WriteLine("El número {0} es {1}", numero, Calculadora.EsPar(numero) ? "par" : "impar");
            }

            Console.ReadLine();
        }
    }
}
