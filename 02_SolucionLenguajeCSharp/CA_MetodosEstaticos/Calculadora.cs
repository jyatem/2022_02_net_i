﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_MetodosEstaticos
{
    public class Calculadora
    {
        public static int Sumar(int o1, int o2)
        {
            return o1 + o2;
        }

        public static bool EsPar(int numero)
        {
            return numero % 2 == 0;
        }
    }
}
