﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Herencia
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Padre padre = new Padre();

            padre.MetodoPadre(10);

            Hija hija = new Hija();

            Padre padre2 = new Hija();
        }
    }
}
