﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Herencia
{
    public class Padre
    {
        private int _informacionPrivada;

        protected string _informacionProtegida;

        public double ProAtributo;

        public string Propiedad1Padre { get; set; }

        public int Propiedad2Padre { get; set; }

        public Padre()
        {
            _informacionPrivada = 0;
        }

        public void MetodoPadre(int informacion)
        {
            Console.WriteLine($"Método del padre {informacion}");
        }
    }
}
