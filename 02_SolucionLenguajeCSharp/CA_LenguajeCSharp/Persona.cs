﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_LenguajeCSharp
{
    public class Persona
    {
        #region [ Propiedades ]
        public string Nombre { get; set; }

        public int Edad 
        {
            get
            {
                int edad = DateTime.Today.AddTicks(-FechaNacimiento.Ticks).Year - 1;
                return edad;
            }
        }

        public DateTime FechaNacimiento { get; set; }

        public bool? Casado { get; set; }

        public Genero GeneroPersona { get; set; }
        #endregion

        #region [ Constructores ]
        public Persona()
        {
        }

        // Sobrecargar a nivel de constructores, decimos que el Constructor va a llenar sólo 2 propiedades
        public Persona(string nombre, int edad)
        {
            Nombre = nombre;
            //Edad = edad;
        }

        public Persona(string nombre, int edad, DateTime fechaNacimiento)
        {
            Nombre = nombre;
            //Edad = edad;
            FechaNacimiento = fechaNacimiento;
        }
        #endregion

        #region [ Métodos ]
        public void MostrarInformacion()
        {
            string casado = "";

            if (Casado.HasValue)
            {
                casado = Casado.Value ? "está casado(a)" : "no está casado(a)";
            }
            else
            {
                casado = "sin definir estado civil";
            }

            #region [ Con un if ]
            //if (Casado.Value)
            //{
            //    casado = "...";
            //}
            //else
            //{
            //    casado = "...";
            //} 
            #endregion

            Console.WriteLine($"{Nombre} tiene {Edad} años, nació el {FechaNacimiento.ToShortDateString()}, es {GeneroPersona} y {casado}");
        }
        #endregion
    }
}
