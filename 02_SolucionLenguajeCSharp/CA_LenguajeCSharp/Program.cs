﻿using System;
using System.Collections.Generic;

namespace CA_LenguajeCSharp
{
    #region Clase Operación de ejemplo
    class Operacion
    {
        public static int Sumar(int operador1, int operador2)
        {
            int s;

            s = operador1 + operador2;

            return s;

            //return operador1 + operador2;
        }
    }
    #endregion

    internal class Program
    {
        int variableGlobal = 1;

        static void Main(string[] args)
        {
            #region [ Código lenguaje C# ]
            int contador = 0;

            //string nombre = "Jairo"; // String declaro el valor en comillas dobles
            char letra = 'a';        // Char declaro el varlo con comillas simples

            Console.WriteLine(contador);

            Console.WriteLine(Operacion.Sumar(10, 5));

            List<string> nombres = new List<string>();
            nombres.Add("Jairo");
            nombres.Add("Lina");
            nombres.Add("Mariana");
            nombres.Add("Juan");

            foreach (string nombre in nombres)
            {
                Console.WriteLine(nombre);
            }
            #endregion

            #region [ Instanciando clases ]
            Persona persona1 = new Persona(); // Hacer una instancia de la clase Persona. persona1 es un objeto de dicha clase.
            persona1.Nombre = "Jairo";
            //persona1.Edad = 42;
            persona1.Casado = true;
            persona1.GeneroPersona = Genero.Masculino;
            persona1.FechaNacimiento = new DateTime(1980, 8, 13);

            Persona persona2 = new Persona
            {
                Nombre = "Lina",
                //Edad = 42,
                Casado = true,
                GeneroPersona = Genero.Femenino,
                FechaNacimiento = Convert.ToDateTime("1979-12-29")
            };

            Persona persona3 = new Persona("Mariana", 16, new DateTime(2006, 2, 14));
            persona3.Casado = false;
            persona3.GeneroPersona = Genero.Femenino;

            Console.WriteLine("Nombre: " + persona1.Nombre + " Edad: " + persona1.Edad + " Fecha de Nacimiento: " + persona1.FechaNacimiento);

            Console.WriteLine("Nombre: {0} Edad: {1} Fecha de Nacimiento: {2}", persona2.Nombre, persona2.Edad, persona2.FechaNacimiento);

            Console.WriteLine($"Nombre: {persona2.Nombre} Edad: {persona2.Edad} Fecha de Nacimiento: {persona2.FechaNacimiento}");

            // 2do constructor le van a agregar la fecha. Instanciar una persona3 llamando a ese constructor. Mostrar la información de persona3. 
            #endregion

            #region [ Nullable y valores null ]
            int? x = null;
            double? y = null;

            x = 2;

            Persona persona4 = null;
            #endregion

            Persona persona5 = new Persona("Manuel", 12, new DateTime(2010, 7, 24));
            persona5.Casado = null;
            persona5.GeneroPersona = Genero.Masculino;

            if (persona5.Casado.HasValue)
            {
                Console.WriteLine("Casado tiene valor y es: " + persona5.Casado);
            }
            else
            {
                Console.WriteLine("Persona 5 casado es null");
            }

            Console.WriteLine(persona5.GeneroPersona);

            // Crear un método en la clase persona, que muestre toda la información de las propiedades. De las 5 instancias que se han creado. Listado generico de persona, en las que van a agregar esas 5 instancias y luego en un foreach usan el método que muestra la información.

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("*************************************************************");            
            //persona1.MostrarInformacion();

            List<Persona> listadoPersonas = new List<Persona>();
            listadoPersonas.Add(persona1);
            listadoPersonas.Add(persona2);
            listadoPersonas.Add(persona3);
            listadoPersonas.Add(persona4);
            listadoPersonas.Add(persona5);

            foreach (var persona in listadoPersonas)
            {
                if (persona != null)
                {
                    persona.MostrarInformacion();
                }
            }

            // Método tiene el modificado de acceso (públic, privado), se retorna algo (no retorna nada es void), el nombre del método CamelCase (LlenarInformacion), y si tiene parametros

            Console.ReadLine();
        }

        #region [ Métodos ]
        private void Metodo1()
        {
            int variableLocalMetodo1 = 2;
            Console.WriteLine(variableGlobal);
            Console.WriteLine(variableLocalMetodo1);
        }

        private void Metodo2()
        {
            int variableLocalMetodo2 = 3;
            string persona = "Jairo";
            Console.WriteLine(variableLocalMetodo2);
        }
        #endregion

    }
}
