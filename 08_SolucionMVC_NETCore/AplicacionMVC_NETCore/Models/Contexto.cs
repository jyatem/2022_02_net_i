﻿using Microsoft.EntityFrameworkCore;

namespace AplicacionMVC_NETCore.Models
{
    public class Contexto : DbContext
    {
        public Contexto(DbContextOptions<Contexto> opciones) : base(opciones)
        {
        }

        public DbSet<Persona> Personas { get; set; }
    }
}
