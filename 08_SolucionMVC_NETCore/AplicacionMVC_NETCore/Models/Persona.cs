﻿using System.ComponentModel.DataAnnotations;

namespace AplicacionMVC_NETCore.Models
{
    public class Persona
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El {0} es requerido")]
        public string Nombre { get; set; }

        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }
    }
}
