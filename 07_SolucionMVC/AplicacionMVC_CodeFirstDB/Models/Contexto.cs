using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace AplicacionMVC_CodeFirstDB.Models
{
    public partial class Contexto : DbContext
    {
        public Contexto()
            : base("name=Contexto")
        {
        }

        public virtual DbSet<Ciudad> Ciudad { get; set; }
        public virtual DbSet<Curso> Curso { get; set; }
        public virtual DbSet<Departamento> Departamento { get; set; }
        public virtual DbSet<Estudiante> Estudiante { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ciudad>()
                .HasMany(e => e.Estudiante)
                .WithRequired(e => e.Ciudad)
                .HasForeignKey(e => e.CiudadNacimiento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Departamento>()
                .HasMany(e => e.Ciudad)
                .WithRequired(e => e.Departamento)
                .HasForeignKey(e => e.IdDepartamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Estudiante>()
                .HasMany(e => e.Curso)
                .WithMany(e => e.Estudiante)
                .Map(m => m.ToTable("CursoEstudiante").MapLeftKey("CedulaEstudiante").MapRightKey("IdCurso"));
        }
    }
}
