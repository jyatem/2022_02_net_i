﻿using AplicaciónMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace AplicaciónMVC.Controllers
{
    public class CorreoController : Controller
    {
        // GET: Correo
        public ActionResult EnviarCorreo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EnviarCorreo(CorreoViewModel correoViewModel)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                
                smtpClient.Credentials = new NetworkCredential("jyatem@gmail.com", "ujkgzxsstmpfuvih");
                smtpClient.EnableSsl = true;

                MailMessage mailMessage = new MailMessage
                {
                    Subject = correoViewModel.Asunto,
                    From = new MailAddress("jyatem@gmail.com", "Yate"),
                    Body = correoViewModel.Cuerpo
                };

                mailMessage.To.Add(new MailAddress(correoViewModel.Destinatario));
                smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return View(correoViewModel);
        }
    }
}