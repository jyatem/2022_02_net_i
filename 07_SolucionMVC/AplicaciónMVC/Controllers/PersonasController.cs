﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AplicaciónMVC.Models;
using PagedList;

namespace AplicaciónMVC.Controllers
{
    public class PersonasController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Personas
        public ActionResult Index(string ordenamiento, string filtroActual, string filtro, int? pagina)
        {
            ViewBag.OrdenamientoActual = ordenamiento;
            ViewBag.NombreOrdenamiento = String.IsNullOrEmpty(ordenamiento) ? "nombre_desc" : "";

            if (filtro != null)
            {
                pagina = 1;
            }
            else
            {
                filtro = filtroActual;
            }

            ViewBag.FiltroActual = filtro;

            // LINQ
            var personas = from p in db.Personas
                           select p;

            // SELECT .... WHERE Nombre LIKE '%...%' OR Apellido LIKE '%...%'
            if (!String.IsNullOrEmpty(filtro))
            {
                personas = personas.Where(p => p.Nombre.Contains(filtro) || p.Apellido.Contains(filtro));
            }
            
            //if (descce)
            //    SELECT ..... ORDER BY Nombre DESC
            //else
            // SELECT..... ORDER BY Nombre
            // EXEC

            switch (ordenamiento)
            {
                case "nombre_desc":
                    personas = personas.OrderByDescending(p => p.Nombre);
                    break;
                default:
                    personas = personas.OrderBy(p => p.Nombre);
                    break;
            }

            int tamannoPagina = 2;
            int numeroPagina = pagina ?? 1;

            //var personas = db.Personas.Include(p => p.TipoDocumento);
            //return View(personas.ToList());
            return View(personas.ToPagedList(numeroPagina, tamannoPagina));
        }

        // GET: Personas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // GET: Personas/Create
        public ActionResult Create()
        {
            ViewBag.TipoDocumentoId = new SelectList(db.TiposDocumento.OrderBy(tipoDocumento => tipoDocumento.Descripcion), "Id", "Descripcion");
            return View();
        }

        // POST: Personas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Apellido,Celular,TelefonoFijo,Correo,Documento,TipoDocumentoId")] Persona persona)
        {
            if (ModelState.IsValid)
            {
                db.Personas.Add(persona);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoDocumentoId = new SelectList(db.TiposDocumento.OrderBy(tipoDocumento => tipoDocumento.Descripcion), "Id", "Descripcion", persona.TipoDocumentoId);
            return View(persona);
        }

        // GET: Personas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoDocumentoId = new SelectList(db.TiposDocumento.OrderBy(tipoDocumento => tipoDocumento.Descripcion), "Id", "Descripcion", persona.TipoDocumentoId);
            return View(persona);
        }

        // POST: Personas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Apellido,Celular,TelefonoFijo,Correo,Documento,TipoDocumentoId")] Persona persona)
        {
            if (ModelState.IsValid)
            {
                db.Entry(persona).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoDocumentoId = new SelectList(db.TiposDocumento.OrderBy(tipoDocumento => tipoDocumento.Descripcion), "Id", "Descripcion", persona.TipoDocumentoId);
            return View(persona);
        }

        // GET: Personas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Persona persona = db.Personas.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Persona persona = db.Personas.Find(id);
            db.Personas.Remove(persona);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
