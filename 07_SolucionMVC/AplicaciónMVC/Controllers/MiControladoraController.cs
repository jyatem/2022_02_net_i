﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AplicaciónMVC.Controllers
{
    public class MiControladoraController : Controller
    {
        public ActionResult Saludo(string nombre)
        {
            ViewBag.Mensaje = $"Hola {nombre}";

            return View();
        }
    }
}