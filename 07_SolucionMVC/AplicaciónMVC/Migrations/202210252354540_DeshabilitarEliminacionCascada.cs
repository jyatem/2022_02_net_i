﻿namespace AplicaciónMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeshabilitarEliminacionCascada : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Personas", "TipoDocumentoId", "dbo.TiposDocumento");
            AddForeignKey("dbo.Personas", "TipoDocumentoId", "dbo.TiposDocumento", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Personas", "TipoDocumentoId", "dbo.TiposDocumento");
            AddForeignKey("dbo.Personas", "TipoDocumentoId", "dbo.TiposDocumento", "Id", cascadeDelete: true);
        }
    }
}
