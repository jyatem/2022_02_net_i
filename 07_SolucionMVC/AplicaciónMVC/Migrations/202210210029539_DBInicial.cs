﻿namespace AplicaciónMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DBInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Apellido = c.String(maxLength: 50),
                        Celular = c.String(maxLength: 20),
                        Correo = c.String(),
                        Documento = c.String(nullable: false, maxLength: 20),
                        TipoDocumentoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TiposDocumento", t => t.TipoDocumentoId, cascadeDelete: true)
                .Index(t => t.TipoDocumentoId);
            
            CreateTable(
                "dbo.TiposDocumento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Personas", "TipoDocumentoId", "dbo.TiposDocumento");
            DropIndex("dbo.Personas", new[] { "TipoDocumentoId" });
            DropTable("dbo.TiposDocumento");
            DropTable("dbo.Personas");
        }
    }
}
