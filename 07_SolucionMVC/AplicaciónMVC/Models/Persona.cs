﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplicaciónMVC.Models
{
    [Table("Personas")]
    public class Persona
    {
        public int Id { get; set; }

        [StringLength(50, ErrorMessage = "El campo {0} debe tener {2} y {1} caracteres", MinimumLength = 3)]
        [Required(ErrorMessage = "Debe ingresar el campo {0}")]
        public string Nombre { get; set; }

        [StringLength(50, ErrorMessage = "El campo {0} debe tener {2} y {1} caracteres", MinimumLength = 3)]
        public string Apellido { get; set; }

        [StringLength(20, ErrorMessage = "El campo {0} debe tener {2} y {1} caracteres", MinimumLength = 10)]
        public string Celular{ get; set; }

        [StringLength(20, ErrorMessage = "El campo {0} debe tener {2} y {1} caracteres", MinimumLength = 10)]
        public string TelefonoFijo { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Correo { get; set; }
        
        [StringLength(20, ErrorMessage = "El campo {0} debe tener {2} y {1} caracteres", MinimumLength = 6)]
        [Required(ErrorMessage = "Debe ingresar el campo {0}")]
        public string Documento { get; set; }

        public int TipoDocumentoId { get; set; }

        public virtual TipoDocumento TipoDocumento { get; set; }
    }
}