﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AplicaciónMVC.Models
{
    [Table("TiposDocumento")]
    public class TipoDocumento
    {
        public int Id { get; set; }

        [Display(Name = "Tipo de documento")]
        [Required(ErrorMessage = "Debe ingresar el campo {0}")]
        public string Descripcion { get; set; }

        public virtual ICollection<Persona> Personas { get; set; }
    }
}