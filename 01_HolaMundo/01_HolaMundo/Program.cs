﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_HolaMundo
{
    //**************************************
    // Nombre: Hola
    // Descrip...
    //**************************************
    internal class Program
    {
        /// <summary>
        /// Este es el método principal y desde acá arranca la aplicación
        /// </summary>
        /// <param name="args">Es un parametro de entrada para colocar valores en el aplicativo</param>
        static void Main(string[] args)
        {
            String nombre = "Jairo";

            // Para ver en Git

            Console.WriteLine("Hola mundo " + nombre);

            // TODO: terminar de implementar el hola mundo, colocando cómo variable mi nombre
            Console.ReadLine();

            /*
             * Este es un comentario
             * de varias líneas
             */
        }
    }
}
