﻿using MVVM.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MVVM.Services
{
    public class PersonaServicio
    {
        private ObservableCollection<Persona> _personas { get; set; }

        public PersonaServicio()
        {
            if (_personas == null)
            {
                _personas = new ObservableCollection<Persona>();
            }
        }

        public ObservableCollection<Persona> RetornarPersonas()
        {
            return _personas;
        }

        public void GuardarPersona(Persona persona)
        {
            if (persona != null)
            {
                if (persona.Id == "")
                {
                    Guid idPersona = Guid.NewGuid();
                    persona.Id = idPersona.ToString();
                    _personas.Add(persona);
                }
                else
                {
                    var personaActualizar = _personas.FirstOrDefault(p => p.Id == persona.Id);
                    int posicion = _personas.IndexOf(personaActualizar);
                    _personas[posicion] = persona;
                }
            }
        }

        public void EliminarPersona(Persona persona)
        {
            var personaEliminar = _personas.FirstOrDefault(p => p.Id == persona.Id);
            _personas.Remove(personaEliminar);
        }
    }
}
