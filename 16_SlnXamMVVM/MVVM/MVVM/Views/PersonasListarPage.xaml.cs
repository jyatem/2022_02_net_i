﻿using MVVM.Models;
using MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PersonasListarPage : ContentPage
    {
        private PersonaViewModel _personaViewModel;

        public PersonasListarPage()
        {
            InitializeComponent();

            if (_personaViewModel == null)
            {
                _personaViewModel = new PersonaViewModel(Navigation);
            }

            BindingContext = _personaViewModel;

            this.Title = "Listado de Personas";

            var toolBarItem = new ToolbarItem
            {
                Text = "+"
            };

            toolBarItem.Clicked += ToolBarItem_Clicked;

            ToolbarItems.Add(toolBarItem);
        }

        private async void ToolBarItem_Clicked(object sender, EventArgs e)
        {
            _personaViewModel.Id = _personaViewModel.Nombre = _personaViewModel.Apellidos = "";
            _personaViewModel.Edad = 0;

            await Navigation.PushAsync(new PersonaPage() { BindingContext = _personaViewModel });
        }

        private async void lsvPersonas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Persona personaSeleccionada = e.SelectedItem as Persona;

            _personaViewModel.Id = personaSeleccionada.Id;
            _personaViewModel.Nombre = personaSeleccionada.Nombre;
            _personaViewModel.Apellidos = personaSeleccionada.Apellidos;
            _personaViewModel.Edad = personaSeleccionada.Edad;

            await Navigation.PushAsync(new PersonaPage() { BindingContext = _personaViewModel });
        }
    }
}