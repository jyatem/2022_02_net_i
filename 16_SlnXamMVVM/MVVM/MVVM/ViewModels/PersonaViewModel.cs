﻿using MVVM.Models;
using MVVM.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MVVM.ViewModels
{
    public class PersonaViewModel : Persona
    {
        public ObservableCollection<Persona> Personas { get; set; }

        private INavigation _navigation;

        private PersonaServicio _personaServicio;

        private Persona _persona;

        public Command GuardarCommand { get; set; }
        public Command EliminarCommand { get; set; }

        public PersonaViewModel(INavigation navigation)
        {
            if (_personaServicio == null)
            {
                _personaServicio = new PersonaServicio();
            }

            _navigation = navigation;

            Personas = _personaServicio.RetornarPersonas();

            GuardarCommand = new Command(async () => await Guardar(), () => !EstaOcupado);
            EliminarCommand = new Command(async () => await Eliminar(), () => !EstaOcupado);
        }

        private async Task Guardar()
        {
            EstaOcupado = true;

            _persona = new Persona
            {
                Id = this.Id,
                Nombre = this.Nombre,
                Apellidos = this.Apellidos,
                Edad = this.Edad
            };

            _personaServicio.GuardarPersona(_persona);

            await Task.Delay(2000);
            await _navigation.PopAsync();

            EstaOcupado = false;
        }

        public async Task Eliminar()
        {
            EstaOcupado = true;

            _personaServicio.EliminarPersona(_persona);

            await Task.Delay(2000);
            await _navigation.PopAsync();

            EstaOcupado = false;
        }
    }
}
