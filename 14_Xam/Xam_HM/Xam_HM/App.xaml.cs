﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xam_HM
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new PageTriqui();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
