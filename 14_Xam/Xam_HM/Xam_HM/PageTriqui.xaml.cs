﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xam_HM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageTriqui : ContentPage
    {
        private Button[] _botones = new Button[9];
        private GestionTriqui _gestionTriqui = new GestionTriqui();

        public PageTriqui()
        {
            InitializeComponent();

            _botones[0] = btn1;
            _botones[1] = btn2;
            _botones[2] = btn3;
            _botones[3] = btn4;
            _botones[4] = btn5;
            _botones[5] = btn6;
            _botones[6] = btn7;
            _botones[7] = btn8;
            _botones[8] = btn9;
        }

        private void btn_Clicked(object sender, EventArgs e)
        {
            _gestionTriqui.SetBoton((Button)sender);

            if (_gestionTriqui.ChequearGanador(_botones))
            {
                slJuegoTermino.IsVisible = true;
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            _gestionTriqui.ResetearJuego(_botones);
            slJuegoTermino.IsVisible = false;
        }
    }
}