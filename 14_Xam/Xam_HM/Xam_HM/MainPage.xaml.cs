﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Xam_HM
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void btnSaludar_Clicked(object sender, EventArgs e)
        {
            await DisplayAlert("Mensaje", $"Hola {txtNombre.Text}!", "Ok");
        }
    }
}
