﻿using AccesoDatos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class LogicaNegocioCurso
    {
        private DAOCurso _daoCurso;

        public LogicaNegocioCurso()
        {
            _daoCurso = new DAOCurso();
        }

        public bool CrearCurso(Curso curso)
        {
            try
            {
                if (String.IsNullOrEmpty(curso.NombreCurso))
                {
                    throw new Exception("El nombre del curso es requerido");
                }

                int resultado = _daoCurso.CrearCurso(curso);

                if (resultado == 1)
                    return true;

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Curso> RetornarCursos()
        {
            try
            {
                return _daoCurso.RetornarCursos();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ActualizarCurso(Curso curso)
        {
            try
            {
                int resultado = _daoCurso.ActualizarCurso(curso);

                if (resultado == 1)
                    return true;

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool EliminarCurso(int id)
        {
            try
            {
                int resultado = _daoCurso.EliminarCurso(id);

                if (resultado > 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AgregarEstudiante(Estudiante estudiante)
        {
        }
    }
}
