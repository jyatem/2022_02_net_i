﻿using AccesoDatos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class LogicaNegocioCiudad
    {
        private DAOCiudad _daoCiudad;

        public LogicaNegocioCiudad()
        {
            _daoCiudad = new DAOCiudad();
        }

        public List<Ciudad> RetornarCiudades(int idDepartamento)
        {
            try
            {
                return _daoCiudad.RetornarCiudades(idDepartamento);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
