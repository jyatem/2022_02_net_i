﻿using AccesoDatos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class LogicaNegocioDepartamento
    {
        private DAODepartamento _daoDepartamento;

        public LogicaNegocioDepartamento()
        {
            _daoDepartamento = new DAODepartamento();
        }
        
        public List<Departamento> RetornarDepartamentos()
        {
            try
            {
                return _daoDepartamento.RetornarDepartamentos();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
