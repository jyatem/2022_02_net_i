﻿using AccesoDatos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaNegocio
{
    public class LogicaNegocioEstudiante
    {
        private DAOEstudiante _daoEstudiante;

        public LogicaNegocioEstudiante()
        {
            _daoEstudiante = new DAOEstudiante();
        }

        public bool CrearEstudiante(Estudiante estudiante)
        {
            try
            {
                if (_daoEstudiante.CrearEstudiante(estudiante) > 0)
                    return true;

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Estudiante> RetornarEstudiantes()
        {
            try
            {
                return _daoEstudiante.RetornarEstudiantes();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool EliminarEstudiante(int cedula)
        {
            try
            {
                // Implementar método de eliminar en el DAO
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
