﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Estudiante
    {
        public int Cedula { get; set; }

        public string Nombre { get; set; }

        public string Apellidos { get; set; }

        public string Correo { get; set; }

        public bool Genero { get; set; }

        public int IdCiudadNacimiento { get; set; }

        public DateTime FechaNacimiento { get; set; }

        public string NombreCiudad { get; set; }

        public string NombreCompleto
        {
            get
            {
                return Nombre + " " + Apellidos;
            }
        }

        public string NombreGenero
        {
            get
            {
                return Genero ? "Masculino" : "Femenino" ;
            }
        }
    }
}
