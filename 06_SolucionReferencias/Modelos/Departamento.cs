﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Departamento
    {
        public int Id { get; set; }

        public string NombreDepartamento { get; set; }
    }
}
