﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm_Universidad
{
    public partial class WebFormBase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDarClic_Click(object sender, EventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(txtNombre.Text);
                lblInformacion.Text = $"Hola {txtNombre.Text}";
            }
            catch (Exception ex)
            {
                lblInformacion.Text = ex.Message;
            }
        }
    }
}