﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormSubirArchivo.aspx.cs" Inherits="WebForm_Universidad.WebFormSubirArchivo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">

        <div class="card">
            <div class="card-header text-white bg-dark">
                Formulario Subir Archivo
            </div>
            <div class="card-body">
                <div class="mb-3">
                    <asp:FileUpload ID="fuArchivo" runat="server" CssClass="form-control" aria-describeby="ayuda"/>
                    <div id="ayuda" class="form-text">Seleccione el archivo a cargar</div>
                </div>
                <asp:Button ID="btnSubir" runat="server" Text="Subir" CssClass="btn btn-dark" OnClick="btnSubir_Click"/>
                <div class="mb-3">
                    <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
