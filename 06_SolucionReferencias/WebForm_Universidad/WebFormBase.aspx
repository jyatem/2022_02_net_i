﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormBase.aspx.cs" Inherits="WebForm_Universidad.WebFormBase" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table border="1">
            <tr>
                <td>
                    Ingrese el nombre:<asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnDarClic" runat="server" Text="Dar clic" OnClick="btnDarClic_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblInformacion" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
