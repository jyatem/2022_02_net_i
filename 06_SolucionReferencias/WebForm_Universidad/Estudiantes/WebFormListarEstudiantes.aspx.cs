﻿using LogicaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm_Universidad.Estudiantes
{
    public partial class WebFormListarEstudiantes : System.Web.UI.Page
    {
        private LogicaNegocioEstudiante _logicaNegocioEstudiante = new LogicaNegocioEstudiante();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                CargarEstudiantes(); 
            }
        }

        protected void gvEstudiantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvEstudiantes.PageIndex = e.NewPageIndex;
            CargarEstudiantes();
        }

        private void CargarEstudiantes()
        {
            gvEstudiantes.DataSource = _logicaNegocioEstudiante.RetornarEstudiantes();
            gvEstudiantes.DataBind();
        }

        protected void gvEstudiantes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Eliminar")
            {
                int cedula = Convert.ToInt32(e.CommandArgument);

                if (_logicaNegocioEstudiante.EliminarEstudiante(cedula))
                {
                    CargarEstudiantes();
                    lblInformacion.CssClass = "text-success";
                    lblInformacion.Text = $"El estudiante se eliminó correctamente ({DateTime.Now.ToString()})";
                }
                else
                {
                    lblInformacion.CssClass = "text-danger";
                    lblInformacion.Text = $"El estudiante con cédula {cedula} no se pudo eliminar ({DateTime.Now.ToString()})";
                }
            }
        }
    }
}