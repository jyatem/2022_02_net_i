﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormListarEstudiantes.aspx.cs" Inherits="WebForm_Universidad.Estudiantes.WebFormListarEstudiantes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="gvEstudiantes" runat="server" CssClass="table table-dark table-striped table-hover mt-3" AutoGenerateColumns="false" AllowPaging="true" PageSize="2" OnPageIndexChanging="gvEstudiantes_PageIndexChanging" OnRowCommand="gvEstudiantes_RowCommand">
        <Columns>
            <asp:HyperLinkField DataTextField="Cedula" HeaderText="Cédula" DataNavigateUrlFormatString="~/Estudiantes/WebFormEstudiante.aspx?Cedula={0}" DataNavigateUrlFields="Cedula" />
            <%--<asp:BoundField HeaderText="Cédula" DataField="Cedula"/>--%>
            <asp:BoundField HeaderText="Nombre del estudiante" DataField="NombreCompleto"/>
            <asp:BoundField HeaderText="Genero" DataField="NombreGenero"/>
            <asp:BoundField HeaderText="Ciudad de Nacimiento" DataField="NombreCiudad"/>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn btn-danger" OnClientClick="return confirm('¿Realmente desea eliminar este estudiante?');" CommandName="Eliminar" CommandArgument='<%# Bind("Cedula") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

    <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>

</asp:Content>
