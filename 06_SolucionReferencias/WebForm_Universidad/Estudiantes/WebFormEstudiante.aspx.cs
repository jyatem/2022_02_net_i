﻿using LogicaNegocio;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm_Universidad.Estudiantes
{
    public partial class WebFormEstudiante : System.Web.UI.Page
    {
        private LogicaNegocioDepartamento _logicaNegocioDepartamento = new LogicaNegocioDepartamento();
        private LogicaNegocioCiudad _logicaNegocioCiudad = new LogicaNegocioCiudad();
        private LogicaNegocioEstudiante _logicaNegocioEstudiante = new LogicaNegocioEstudiante();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (!String.IsNullOrEmpty(Request["Cedula"]))
                    {
                        // Cargar la información del estudiante
                        // Estudiante estudiante = _logicaNegocioEstudiante.RetornarEstudiante(Convert.ToInt32(Request["Cedula"]));
                        txtCedula.Text = Request["Cedula"];
                        txtCedula.Enabled = false;
                    }

                    ddlDepartamento.DataSource = _logicaNegocioDepartamento.RetornarDepartamentos();
                    ddlDepartamento.DataValueField = "Id";
                    ddlDepartamento.DataTextField = "NombreDepartamento";
                    ddlDepartamento.DataBind();
                    ddlDepartamento.Items.Insert(0, new ListItem("-- Seleccione un departamento --", ""));
                }

            }
            catch (Exception ex)
            {
                lblInformacion.Text = ex.Message;
                lblInformacion.CssClass = "text-danger";
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Estudiante estudiante = new Estudiante
                    {
                        Cedula = Convert.ToInt32(txtCedula.Text),
                        Nombre = txtNombre.Text,
                        Apellidos = txtApellido.Text,
                        Correo = txtCorreo.Text,
                        Genero = Convert.ToBoolean(rdoGenero.SelectedValue),
                        IdCiudadNacimiento = Convert.ToInt32(ddlCiudad.SelectedValue),
                        FechaNacimiento = Convert.ToDateTime(txtFecha.Text)
                    };

                    //if (!txtCedula.Enabled)
                    //{
                    if (_logicaNegocioEstudiante.CrearEstudiante(estudiante))
                    {
                        lblInformacion.Text = "El estudiante se ingresó exitosamente";
                        lblInformacion.CssClass = "text-success";

                        txtCedula.Text = txtNombre.Text = txtApellido.Text = txtCorreo.Text = ddlDepartamento.SelectedValue = ddlCiudad.SelectedValue = "";
                        rdoGenero.SelectedValue = "false";
                    }
                    else
                    {
                        lblInformacion.Text = "No se pudo ingresar el estudiante";
                        lblInformacion.CssClass = "text-danger";
                    }
                    //}
                    // Actualizar
                    //else 
                    //{ 
                    //}
                }
            }
            catch (Exception ex)
            {
                lblInformacion.Text = ex.Message;
                lblInformacion.CssClass = "text-danger";
            }
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlCiudad.DataSource = _logicaNegocioCiudad.RetornarCiudades(Convert.ToInt32(ddlDepartamento.SelectedValue));
                ddlCiudad.DataValueField = "Id";
                ddlCiudad.DataTextField = "NombreCiudad";
                ddlCiudad.DataBind();
                ddlCiudad.Items.Insert(0, new ListItem("-- Seleccione una ciudad --", ""));
            }
            catch (Exception ex)
            {
                lblInformacion.Text = ex.Message;
                lblInformacion.CssClass = "text-danger";
            }
        }
    }
}