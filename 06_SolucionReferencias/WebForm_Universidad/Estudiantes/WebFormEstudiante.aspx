﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormEstudiante.aspx.cs" Inherits="WebForm_Universidad.Estudiantes.WebFormEstudiante" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">

        <div class="card">
            <div class="card-header text-white bg-dark">
                Estudiante
            </div>
            <div class="card-body">
                <!-- Cédula -->
                <div class="mb-3">
                    <label for="txtCedula" class="form-label">Cédula</label>
                    <asp:TextBox ID="txtCedula" runat="server" CssClass="form-control" Placeholder="Ingrese la cédula" MaxLength="20"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCedula" runat="server" ErrorMessage="La cédula es requerida" ControlToValidate="txtCedula" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvCedula" runat="server" ErrorMessage="La cédula debe ser un número entero" ControlToValidate="txtCedula" Operator="DataTypeCheck" Type="Integer" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:CompareValidator>
                </div>

                <!-- Nombre -->
                <div class="mb-3">
                    <label for="txtNombre" class="form-label">Nombre</label>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" Placeholder="Ingrese el nombre" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNombre" runat="server" ErrorMessage="El nombre es requerido" ControlToValidate="txtNombre" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revNombre" runat="server" ErrorMessage="Sólo letras y espacio en blanco" ControlToValidate="txtNombre" ValidationExpression="^[ A-Za-zÑñáéíóú]+$" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RegularExpressionValidator>
                </div>

                <!-- Apellidos -->
                <div class="mb-3">
                    <label for="txtApellido" class="form-label">Apellidos</label>
                    <asp:TextBox ID="txtApellido" runat="server" CssClass="form-control" Placeholder="Ingrese los apellidos" MaxLength="250"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revApellido" runat="server" ErrorMessage="Sólo letras y espacio en blanco" ControlToValidate="txtApellido" ValidationExpression="^[ A-Za-zÑñáéíóú]+$" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RegularExpressionValidator>
                </div>

                <!-- Correo -->
                <div class="mb-3">
                    <label for="txtCorreo" class="form-label">Correo</label>
                    <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control" Placeholder="Ingrese el correo" MaxLength="100"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="revCorreo" runat="server" ErrorMessage="El correo no es válido" ControlToValidate="txtCorreo" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RegularExpressionValidator>
                </div>

                <!-- Genero -->
                <div class="mb-3">
                    <label class="form-label">Género</label>
                    <asp:RadioButtonList ID="rdoGenero" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Femenino" Value="false" Selected="True" />
                        <asp:ListItem Text="Masculino" Value="True" />
                    </asp:RadioButtonList>
                </div>

                <!-- Departamento -->
                <div class="mb-3">
                    <label class="form-label">Departamento</label>
                    <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvDepartamento" runat="server" ErrorMessage="El departamento es requerido" ControlToValidate="ddlDepartamento" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RequiredFieldValidator>
                </div>

                <!-- Ciudad -->
                <div class="mb-3">
                    <label class="form-label">Ciudad</label>
                    <asp:DropDownList ID="ddlCiudad" runat="server" CssClass="form-control"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvCiudad" runat="server" ErrorMessage="La ciudad es requerida" ControlToValidate="ddlCiudad" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RequiredFieldValidator>
                </div>

                <!-- Fecha de nacimiento -->
                <div class="mb-3">
                    <label for="txtFecha" class="form-label">Fecha de Nacimiento</label>
                    <asp:TextBox ID="txtFecha" runat="server" CssClass="form-control" Placeholder="Ingrese la fecha" type="date"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvFecha" runat="server" ErrorMessage="La fecha de nacimiento es requerida" ControlToValidate="txtFecha" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"></asp:RequiredFieldValidator>
                </div>
                
                <!-- Botón -->
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-danger" OnClick="btnGuardar_Click"/>

                <!-- Label Información -->
                <div class="mb-3">
                    <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
