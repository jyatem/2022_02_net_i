﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm_Universidad
{
    public partial class WebFormEnviarCorreo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                smtpClient.Credentials = new NetworkCredential("jyatem@gmail.com", "ujkgzxsstmpfuvih");
                smtpClient.EnableSsl = true;

                MailMessage mailMessage = new MailMessage();

                mailMessage.Subject = txtAsunto.Text;
                mailMessage.To.Add(new MailAddress(txtPara.Text));
                mailMessage.From = new MailAddress("jyatem@gmail.com", "Yate");
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = $"<h1 style='color:blue'>{txtMensaje.Text}</h1>";

                // Incluir un adjunto al correo
                string rutaArchivo = Server.MapPath("~/Archivos/Ejercicio Diagrama Clases.png");
                Attachment attachment = new Attachment(rutaArchivo);
                mailMessage.Attachments.Add(attachment);

                // Enviar correo
                smtpClient.Send(mailMessage);

                lblInformacion.Text = "Correo enviado exitosamente";
                lblInformacion.CssClass = "text-success";
            }
            catch (Exception ex)
            {
                lblInformacion.Text = ex.Message;
                lblInformacion.CssClass = "text-danger";
            }
        }
    }
}