﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maestra.Master" AutoEventWireup="true" CodeBehind="WebFormEnviarCorreo.aspx.cs" Inherits="WebForm_Universidad.WebFormEnviarCorreo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="mt-3">

        <div class="card">
            <div class="card-header text-white bg-dark">
                Formulario Enviar Correo
            </div>
            <div class="card-body">
                <!-- Para -->
                <div class="mb-3">
                    <label for="txtPara" class="form-label">Para</label>
                    <asp:TextBox ID="txtPara" runat="server" CssClass="form-control" Placeholder="Ingrese el para"></asp:TextBox>
                </div>
                <!-- Asunto -->
                <div class="mb-3">
                    <label for="txtAsunto" class="form-label">Asunto</label>
                    <asp:TextBox ID="txtAsunto" runat="server" CssClass="form-control" Placeholder="Ingrese el asunto"></asp:TextBox>
                </div>
                <!-- Cuerpo -->
                <div class="mb-3">
                    <label for="txtMensaje" class="form-label">Mensaje</label>
                    <asp:TextBox ID="txtMensaje" runat="server" CssClass="form-control" Placeholder="Ingrese el mensaje" TextMode="MultiLine"></asp:TextBox>
                </div>
                <!-- Botón -->
                <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="btn btn-danger" OnClick="btnEnviar_Click"/>
                <!-- Label Información -->
                <div class="mb-3">
                    <asp:Label ID="lblInformacion" runat="server" Text="" CssClass="text-success"></asp:Label>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
