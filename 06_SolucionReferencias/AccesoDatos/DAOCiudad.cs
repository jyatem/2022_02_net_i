﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAOCiudad
    {
        public List<Ciudad> RetornarCiudades(int idDepartamento)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = $"SELECT Id, NombreCiudad FROM Ciudad WHERE IdDepartamento = {idDepartamento} ORDER BY NombreCiudad";

                    conexion.Open();

                    SqlDataReader sqlDataReader = comando.ExecuteReader();
                    List<Ciudad> listadoCiudades = new List<Ciudad>();

                    while (sqlDataReader.Read())
                    {
                        Ciudad ciudad = new Ciudad()
                        {
                            Id = Convert.ToInt32(sqlDataReader["Id"]),
                            NombreCiudad = Convert.ToString(sqlDataReader["NombreCiudad"])
                        };

                        listadoCiudades.Add(ciudad);
                    }

                    sqlDataReader.Close();
                    conexion.Close();

                    return listadoCiudades;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
