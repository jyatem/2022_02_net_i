﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAOEstudiante
    {
        public int CrearEstudiante(Estudiante estudiante)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    int genero = estudiante.Genero ? 1 : 0;
                    
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = $"INSERT INTO Estudiante (Cedula, Nombre, Apellidos, Correo, Genero, CiudadNacimiento, FechaNacimiento) VALUES ({estudiante.Cedula}, '{estudiante.Nombre}', '{estudiante.Apellidos}', '{estudiante.Correo}', {genero}, {estudiante.IdCiudadNacimiento}, '{estudiante.FechaNacimiento:yyyy-MM-dd}')";

                    conexion.Open();
                    int resultado = comando.ExecuteNonQuery();
                    conexion.Close();

                    return resultado;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Estudiante> RetornarEstudiantes()
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "SELECT Cedula, Nombre, Apellidos, Correo, Genero, C.NombreCiudad, FechaNacimiento FROM Estudiante AS E INNER JOIN Ciudad AS C ON E.CiudadNacimiento = C.Id";

                    conexion.Open();

                    SqlDataReader sqlDataReader = comando.ExecuteReader();
                    List<Estudiante> estudiantes = new List<Estudiante>();

                    while (sqlDataReader.Read())
                    {
                        Estudiante estudiante = new Estudiante()
                        {
                            Cedula = Convert.ToInt32(sqlDataReader["Cedula"]),
                            Nombre = Convert.ToString(sqlDataReader["Nombre"]),
                            Apellidos = Convert.ToString(sqlDataReader["Apellidos"]),
                            Correo = Convert.ToString(sqlDataReader["Correo"]),
                            Genero = Convert.ToBoolean(sqlDataReader["Genero"]),
                            NombreCiudad = Convert.ToString(sqlDataReader["NombreCiudad"]),
                            FechaNacimiento = Convert.ToDateTime(sqlDataReader["FechaNacimiento"])
                        };

                        estudiantes.Add(estudiante);
                    }

                    sqlDataReader.Close();
                    conexion.Close();

                    return estudiantes;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
