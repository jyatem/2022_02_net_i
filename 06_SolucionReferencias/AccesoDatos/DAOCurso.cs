﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAOCurso
    {
        public int CrearCurso(Curso curso)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = $"INSERT INTO Curso (NombreCurso, Duracion) VALUES ('{curso.NombreCurso}', {curso.Duracion})";

                    conexion.Open();
                    int resultado = comando.ExecuteNonQuery();
                    conexion.Close();

                    return resultado;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Curso> RetornarCursos()
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "SELECT Id, NombreCurso, Duracion FROM Curso";

                    conexion.Open();

                    SqlDataReader sqlDataReader = comando.ExecuteReader();
                    List<Curso> cursos = new List<Curso>();

                    while (sqlDataReader.Read())
                    {
                        //if (sqlDataReader["Duracion"] != DBNull.Value)
                        //Podría tomar el valor                            
                        
                        Curso curso = new Curso()
                        {
                            Id = Convert.ToInt32(sqlDataReader["Id"]),
                            NombreCurso = Convert.ToString(sqlDataReader["NombreCurso"]),
                            Duracion = Convert.ToInt32(sqlDataReader["Duracion"])
                        };

                        cursos.Add(curso);
                    }

                    sqlDataReader.Close();
                    conexion.Close();

                    return cursos;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int ActualizarCurso(Curso curso)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandType = CommandType.StoredProcedure;
                    comando.CommandText = "ActualizarCurso";

                    SqlParameter sqlParameterId = new SqlParameter("@Id", SqlDbType.Int);
                    SqlParameter sqlParameterNombreCurso = new SqlParameter("@NombreCurso", SqlDbType.NVarChar, 250);
                    SqlParameter sqlParameterDuracion = new SqlParameter("@Duracion", SqlDbType.Int);

                    sqlParameterId.Value = curso.Id;
                    sqlParameterNombreCurso.Value = curso.NombreCurso;
                    sqlParameterDuracion.Value = curso.Duracion;

                    comando.Parameters.Add(sqlParameterId);
                    comando.Parameters.Add(sqlParameterNombreCurso);
                    comando.Parameters.Add(sqlParameterDuracion);

                    conexion.Open();

                    int resultado = comando.ExecuteNonQuery();

                    conexion.Close();

                    return resultado;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int EliminarCurso(int id)
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = $"DELETE FROM Curso WHERE Id = {id}";

                    conexion.Open();
                    int resultado = comando.ExecuteNonQuery();
                    conexion.Close();

                    return resultado;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
