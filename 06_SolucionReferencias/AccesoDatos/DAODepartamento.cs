﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class DAODepartamento
    {
        public List<Departamento> RetornarDepartamentos()
        {
            try
            {
                using (SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionDB"].ConnectionString))
                {
                    SqlCommand comando = new SqlCommand();
                    comando.Connection = conexion;
                    comando.CommandText = "SELECT Id, NombreDepartamento FROM Departamento ORDER BY NombreDepartamento";

                    conexion.Open();

                    SqlDataReader sqlDataReader = comando.ExecuteReader();
                    List<Departamento> listadoDepartamentos = new List<Departamento>();

                    while (sqlDataReader.Read())
                    {
                        Departamento departamento = new Departamento()
                        {
                            Id = Convert.ToInt32(sqlDataReader["Id"]),
                            NombreDepartamento = Convert.ToString(sqlDataReader["NombreDepartamento"])
                        };

                        listadoDepartamentos.Add(departamento);
                    }

                    sqlDataReader.Close();
                    conexion.Close();

                    return listadoDepartamentos;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
