﻿using LogicaNegocio;
using Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Universidad
{
    public partial class FormCurso : Form
    {
        private LogicaNegocioCurso _logicaNegocioCurso = new LogicaNegocioCurso();

        public FormCurso()
        {
            InitializeComponent();
        }

        private void FormCurso_Load(object sender, EventArgs e)
        {
            try
            {
                dgvCursos.DataSource = _logicaNegocioCurso.RetornarCursos();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarCurso();
        }

        private void dgvCursos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvCursos.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                if (e.ColumnIndex == 0)
                {
                    dgvCursos.CurrentRow.Selected = true;
                    txtId.Text = dgvCursos.Rows[e.RowIndex].Cells["Id"].Value.ToString();
                    txtNombre.Text = dgvCursos.Rows[e.RowIndex].Cells["NombreCurso"].Value.ToString();
                    txtDuracion.Text = dgvCursos.Rows[e.RowIndex].Cells["Duracion"].Value.ToString();
                }
                else if (e.ColumnIndex == 1)
                {
                    if (MessageBox.Show("¿Realmente desea eliminar el curso?", "Eliminar Curso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        if (_logicaNegocioCurso.EliminarCurso(Convert.ToInt32(dgvCursos.Rows[e.RowIndex].Cells["Id"].Value)))
                        {
                            MessageBox.Show("Curso eliminado exitosamente");
                            dgvCursos.DataSource = _logicaNegocioCurso.RetornarCursos();
                        }
                    }
                }
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Curso curso = new Curso()
                {
                    Id = Convert.ToInt32(txtId.Text),
                    NombreCurso = txtNombre.Text,
                    Duracion = Convert.ToInt32(txtDuracion.Text)
                };

                if (_logicaNegocioCurso.ActualizarCurso(curso))
                {
                    MessageBox.Show("Curso actualizado exitosamente", "Actualizar Curso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvCursos.DataSource = _logicaNegocioCurso.RetornarCursos();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GuardarCurso();
        }

        private void GuardarCurso()
        {
            try
            {
                // Validar si el campo txtDuracion tiene un número entero
                if (String.IsNullOrEmpty(txtDuracion.Text))
                {
                    errorProvider.SetError(txtDuracion, "Se debe ingresar la duración");
                    return;
                }

                int duracion = 0;

                if (!int.TryParse(txtDuracion.Text, out duracion))
                {
                    errorProvider.SetError(txtDuracion, "La duración debe ser un número entero");
                    return;
                }

                errorProvider.SetError(txtDuracion, "");

                Curso curso = new Curso()
                {
                    NombreCurso = txtNombre.Text,
                    Duracion = duracion
                };

                if (_logicaNegocioCurso.CrearCurso(curso))
                {
                    MessageBox.Show("Curso creado exitosamente");
                    dgvCursos.DataSource = _logicaNegocioCurso.RetornarCursos();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
