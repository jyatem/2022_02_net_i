﻿using LogicaNegocio;
using Modelos;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_Universidad
{
    public partial class FormEstudiante : Form
    {
        private LogicaNegocioDepartamento _logicaNegocioDepartamento = new LogicaNegocioDepartamento();
        private LogicaNegocioCiudad _logicaNegocioCiudad = new LogicaNegocioCiudad();

        public FormEstudiante()
        {
            InitializeComponent();            
        }

        private void FormEstudiante_Load(object sender, EventArgs e)
        {
            try
            {
                List<Departamento> listadoDepartamentos = _logicaNegocioDepartamento.RetornarDepartamentos();

                listadoDepartamentos.Insert(0, new Departamento { Id = 0, NombreDepartamento = "-- Seleccione un departamento --" });

                cmbDepartamentos.ValueMember = "Id";
                cmbDepartamentos.DisplayMember = "NombreDepartamento";
                cmbDepartamentos.DataSource = listadoDepartamentos;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!cmbDepartamentos.SelectedValue.ToString().Equals("0"))
                {
                    List<Ciudad> listadoCiudades = _logicaNegocioCiudad.RetornarCiudades(Convert.ToInt32(cmbDepartamentos.SelectedValue));

                    listadoCiudades.Insert(0, new Ciudad { Id = 0, NombreCiudad = "-- Seleccione una ciudad --" });

                    cmbCiudades.ValueMember = "Id";
                    cmbCiudades.DisplayMember = "NombreCiudad";
                    cmbCiudades.DataSource = listadoCiudades;
                }
                else
                {
                    cmbCiudades.DataSource = new List<Ciudad>();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                Estudiante estudiante = new Estudiante
                {
                    Cedula = Convert.ToInt32(txtCedula.Text),
                    Nombre = txtNombre.Text,
                    Apellidos = txtApellidos.Text,
                    Correo = txtCorreo.Text,
                    Genero = rdoFemenino.Checked,
                    IdCiudadNacimiento = Convert.ToInt32(cmbCiudades.SelectedValue),
                    FechaNacimiento = dtpFechaNacimiento.Value
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
