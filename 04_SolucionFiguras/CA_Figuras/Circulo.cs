﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Circulo : Figura
    {
        public int Radio { get; set; }

        public Circulo(Color colorFigura) : base(colorFigura)
        {
        }

        public Circulo(Color colorFigura, int radio) : base(colorFigura)
        {
            Radio = radio;
        }

        public override void Pintar()
        {
            //base.Pintar();
            Console.WriteLine($"Pinta un círculo de color {ColorFigura}");
        }
    }
}
