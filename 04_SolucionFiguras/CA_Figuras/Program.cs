﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Figura> listadoFiguras = new List<Figura>();

            listadoFiguras.Add(new Figura(Color.Amarillo));
            listadoFiguras.Add(new Circulo(Color.Rojo));
            listadoFiguras.Add(new Linea(Color.Azul));
            listadoFiguras.Add(new Circulo(Color.Azul, 10));

            // Polimorfismo: toma una forma en tiempo de ejecución
            foreach (var figura in listadoFiguras)
            {
                figura.Pintar();
            }

            Console.ReadLine();
        }
    }
}
