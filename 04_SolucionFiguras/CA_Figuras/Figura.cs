﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Figura
    {
        public Color ColorFigura { get; set; }

        public Figura(Color colorFigura)
        {
            ColorFigura = colorFigura;
        }

        // Declaro un método virtual, significa que sus hijos pueden sobreescribir el método. Puede conservar el código que propone el padre.
        public virtual void Pintar()
        {
            Console.WriteLine($"Pinta una figura de color {ColorFigura}");
        }
    }
}
