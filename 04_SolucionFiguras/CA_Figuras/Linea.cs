﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Figuras
{
    public class Linea : Figura
    {
        public Linea(Color colorFigura) : base(colorFigura)
        {
        }

        public override void Pintar()
        {
            Console.WriteLine($"Pinta una línea de color {ColorFigura}");
        }
    }
}
