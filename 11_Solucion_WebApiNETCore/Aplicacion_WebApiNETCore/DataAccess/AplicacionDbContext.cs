﻿using Aplicacion_WebApiNETCore.Models;
using Microsoft.EntityFrameworkCore;

namespace Aplicacion_WebApiNETCore.DataAccess
{
    public class AplicacionDbContext : DbContext
    {
        public AplicacionDbContext(DbContextOptions<AplicacionDbContext> options) : base(options)
        {
        }

        public DbSet<Programa> Programas { get; set; }

        public DbSet<Curso> Cursos { get; set; }
    }
}
