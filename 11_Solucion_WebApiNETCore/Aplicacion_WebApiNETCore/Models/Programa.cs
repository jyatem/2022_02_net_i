﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aplicacion_WebApiNETCore.Models
{
    [Table("Programas")]
    public class Programa
    {
        public int Id { get; set; }

        [StringLength(100, ErrorMessage = "El campo {0} debe tener entre {2} y {1} caracteres", MinimumLength = 5)]
        [Required(ErrorMessage = "Debe ingresar el campo {0}")]
        public string NombrePrograma { get; set; }

        public virtual ICollection<Curso>? Cursos { get; set; }
    }
}
