﻿namespace Aplicacion_WebApiNETCore.DTOs
{
    public class CursoDTO
    {
        public int Id { get; set; }

        public string NombreCurso { get; set; }

        public string NombrePrograma { get; set; }
    }
}
